﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace AutoImport.Models
{
    public class RootConfigure
    {
        [JsonPropertyName("settings")]
        public List<Configure> Settings { get; set; }
    }

    public class Configure
    {
        [JsonPropertyName("sql")]
        public SqlConfigure Sql { get; set; }
        [JsonPropertyName("ftp")]
        public FtpConfigure Ftp { get; set; }
        [JsonPropertyName("url")]
        public string Url { get; set; }
    }

    public class SqlConfigure
    {
        [JsonPropertyName("server")]
        public string Server { get; set; }
        [JsonPropertyName("database")]
        public string Database { get; set; }
        [JsonPropertyName("port")]
        public string Port { get; set; }
        [JsonPropertyName("user")]
        public string User { get; set; }
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }

    public class FtpConfigure
    {
        [JsonPropertyName("server")]
        public string Server { get; set; }
        [JsonPropertyName("user")]
        public string User { get; set; }
        [JsonPropertyName("password")]
        public string Password { get; set; }
    }
}
