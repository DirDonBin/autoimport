﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutoImport.Models
{
    public class XmlImportModel
    {
        public XmlImportModel()
        {
            Categories = new List<Category>();
            Offers = new List<Offer>();
        }

        public List<Category> Categories { get; set; }
        public List<Offer> Offers { get; set; }
    }

    public class Category
    {
        public int Id { get; set; }
        public int? ParrentId { get; set; }
        public string Name { get; set; }
    }

    public class Offer
    {
        public Offer()
        {
            Pictures = new List<string>();
            Params = new Dictionary<string, string>();
        }

        public string Id { get; set; }
        public bool Available { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
        public int CategoryId { get; set; }
        public int Price { get; set; }
        public List<string> Pictures { get; set; }
        public string VendorCode { get; set; }
        public Dictionary<string, string> Params { get; set; }
        public string Description { get; set; }
    }
}
