﻿using AutoImport.Import;
using AutoImport.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace AutoImport
{
    class Program
    {
        private static DateTime TaskTime = new DateTime(2020, 04, 27, 16, 0, 0);
        private static bool IsChangeTime = true;

        static void Main(string[] args)
        {
            while (true)
            {
                DateTime now = DateTime.Now;
                
                //if ((now - TaskTime).TotalHours > 0)
                //    TaskTime = new DateTime(now.Year, now.Month, now.Day + 1, TaskTime.Hour, TaskTime.Minute, TaskTime.Second);
                    
                var diff = (TaskTime - now).TotalSeconds;
                if (diff <= 0)
                {
                    var configure = GetConfigurations();

                    foreach (var config in configure)
                    {
                        var th = new Thread(() => ImportFromXml.Import(config));
                        th.Start();
                    }
                }

                Thread.Sleep(1000 * 60 * 5);
            }
        }

        private static List<Configure> GetConfigurations()
        {
            var file = "Configure.json";

            using (StreamReader r = new StreamReader(file))
            {
                string json = r.ReadToEnd();
                RootConfigure items = JsonConvert.DeserializeObject<RootConfigure>(json);
                return items.Settings;
            }
        }
    }
}
