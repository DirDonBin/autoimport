﻿using AutoImport.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace AutoImport.Import
{
    public static class ImportFromXml
    {
        public static void Import(Configure config)
        {
            var connString = $"server={config.Sql.Server};port={config.Sql.Port};Database={config.Sql.Database};uid={config.Sql.User};pwd={config.Sql.Password}";

            try
			{
                var connect = new MySql.Data.MySqlClient.MySqlConnection();
                connect.ConnectionString = connString;
                connect.Open();
                if (connect.State != System.Data.ConnectionState.Open)
                {
                    throw new Exception("Database Not Oppened.");
                }

                if (string.IsNullOrWhiteSpace(config.Url)) return;

                var products = new XmlImportModel();

                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(config.Url);

                var xCategories = xDoc.GetElementsByTagName("category");
                foreach (XmlNode xCategory in xCategories)
                {
                    products.Categories.Add(new Category
                    {
                        Id = int.Parse(xCategory.Attributes.GetNamedItem("id")?.Value ?? "0"),
                        ParrentId = int.TryParse(xCategory.Attributes.GetNamedItem("parentId")?.Value, out var tempVal) ? tempVal : (int?)null,
                        Name = xCategory.InnerText
                    });
                }

                var xOffers = xDoc.GetElementsByTagName("offer");
                foreach (XmlNode xOffer in xOffers)
                {
                    var offer = new Offer
                    {
                        Available = bool.TryParse(xOffer.Attributes.GetNamedItem("available")?.Value, out var tempVal) ? tempVal : true,
                        Id = xOffer.Attributes.GetNamedItem("id")?.Value
                    };

                    foreach (XmlNode xOfferTag in xOffer.ChildNodes)
                    {
                        switch (xOfferTag.Name)
                        {
                            case "name":
                                offer.Name = xOfferTag.InnerText;
                                break;
                            case "categoryId":
                                var id = int.Parse(xOfferTag.InnerText);
                                offer.CategoryId = id;
                                offer.Category = products.Categories.FirstOrDefault(x => x.Id == id);
                                break;
                            case "price":
                                offer.Price = int.Parse(xOfferTag.InnerText ?? "0");
                                break;
                            case "picture":
                                offer.Pictures.Add(xOfferTag.InnerText);
                                break;
                            case "vendorCode":
                                offer.VendorCode = xOfferTag.InnerText;
                                break;
                            case "param":
                                offer.Params.Add(xOfferTag.Attributes.GetNamedItem("name")?.Value, xOffer.InnerText);
                                break;
                            case "description":
                                offer.Description = xOfferTag.InnerText;
                                break;
                            default:
                                break;
                        }
                    }

                    products.Offers.Add(offer);
                }

                //ftp
            }
			catch (Exception ex)
			{
                Console.WriteLine($"{connString} |||| {config.Url} || Error{ex.Message}");
                return;
			}
        }
    }
}
